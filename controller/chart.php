<?php

    // get the maximun and min date range in the excel
    $file = '/data/export.csv';
    $cmd = 'sed 1d '.$file.' | awk \'{ if($2 > MAX) { MAX=$2} if(($2 < MIN) || MIN =="") {MIN = $2}} END{print MIN"|"MAX }\' FS=";"';

    $maxNMinDate = shell_exec($cmd);

    $maxNMinDateRange = explode('|',$maxNMinDate);

    $minDate = trim($maxNMinDateRange[0]);
    $maxDate = trim($maxNMinDateRange[1]);

    $start = new DateTime($minDate);
    $end = new DateTime($maxDate);
    $interval = new DateInterval('P1D');
    $dateRange = new DatePeriod($start, $interval, $end);

    
    $weekNumber = 1;
    $weeks = array();
    foreach ($dateRange as $date) {
        if ($date->format('w') == 6) {
            $end = $date->format('Y-m-d');
            $weekStartD = $date->modify('-6 days');
            $start = $weekStartD->format('Y-m-d');
            $weeks[] = [$start , $end];

            $period = new DatePeriod(
                new DateTime($start),
                new DateInterval('P1D'),
                new DateTime($end)
           );
            foreach ($period as $key => $value) {
                $chartArray[$start][$value->format('Y-m-d')] = 0;       
            }
        }
    }

    $returnArray = [];
    $dateArray   = [];

    // read the csv file
    $arrayValueOfCsv = shell_exec('cat '.$file.'  | cut -f1');
    $arrayValueOfCsv = explode("\n", $arrayValueOfCsv);

    array_shift($arrayValueOfCsv);

    foreach ($arrayValueOfCsv as $key => $value) {
        $val  = explode(';',$value);
        $date = $val[1];
        if(!isset($dateArray[$date])){
            $dateArray[$date] = ['count' => 1 , 'application' => $val[3] , 'accepted' => $val[4], 'presentage' => $val[2] , 'allPresentage' => $val[2]];
        }else{
            $dateValues    = $dateArray[$date];
            $count         = $dateValues['count'] + 1;
            $application   = $dateValues['application'] + $val[3];
            $accepted      = $dateValues['accepted'] + $val[4];
            $allPresentage = $dateValues['allPresentage'] + $val[2];
            $presentage    = number_format(($allPresentage / $count),0);

            $dateArray[$date] = ['count' => $count , 'application' => $application , 'accepted' => $accepted, 'presentage' => $presentage , 'allPresentage' => $allPresentage];
        }

        foreach ($weeks as $weekKey => $week) {
            $startDate = strtotime($week[0]);
            $endDate = strtotime($week[1]);
            $udate = strtotime($date);

            if(($udate >= $startDate) && ($udate < $endDate)){
                $chartArray[$week[0]][$date] = $dateArray[$date];
            }
        }
    }
    
    foreach ($chartArray as $ckey => $cvalues) {
        foreach ($cvalues as $key => $cvalu) {
            $applicationval[] = ($cvalu['application'] == NULL)? 0: (int)$cvalu['application'];
            $acceptedval[] = ($cvalu['accepted'] == NULL)? 0: (int)$cvalu['accepted'];
            $presentageval[] = ($cvalu['presentage'] == NULL)? 0: (int)$cvalu['presentage'];
        }

        $chartKeyValues['application'][] = ['name' => $ckey, 'data' => $applicationval];
        $chartKeyValues['accepted'][]    = ['name' => $ckey, 'data' => $acceptedval];
        $chartKeyValues['presentage'][]  = ['name' => $ckey, 'data' => $presentageval];
    }
    
    echo  json_encode(['success' => true , 'data' => $chartKeyValues]); exit;
?>